﻿using System;
using System.Windows;

namespace eval_pendu
{
    /// <summary>
    /// Interaction logic for Statistics.xaml
    /// </summary>
    public partial class Statistics : Window
    {
        #region Constructor

        public Statistics()
        {
            InitializeComponent();

            // Do not allow reset if the game is currently running
            BtnReset.IsEnabled = !MainWindow.IsGameRunning;

            // Show data
            DisplayStatisticsValues();
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Put stat data in window fields
        /// </summary>
        private void DisplayStatisticsValues()
        {
            LblGlobalTotalNbGamesStats.Content = MainWindow.StatsGlobalTotalNbGames;
            LblGlobalTotalWinsStats.Content = MainWindow.StatsGlobalTotalWins;
            LblGlobalTotalLossesStats.Content = MainWindow.StatsGlobalTotalLosses;
            LblGlobalBestTimeStats.Content = TimeSpan.FromSeconds(MainWindow.StatsGlobalBestTime);
            LblGlobalLastWordPlayedStats.Content = MainWindow.StatsGlobalLastWordPlayed;
        }

        #endregion

        #region Events

        /// <summary>
        /// When click event occurs on the object, we reset the stat data to their default values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Êtes-vous sûr de vouloir remettre les statistiques à zéro ?", 
                                                        "Remise à zéro", 
                                                        MessageBoxButton.YesNo, 
                                                        MessageBoxImage.Warning,
                                                        MessageBoxResult.No);
            if(result == MessageBoxResult.Yes)
            {
                MainWindow.StatisticsInitProperties();
                DisplayStatisticsValues();
            }
                
        }

        #endregion
    }
}
