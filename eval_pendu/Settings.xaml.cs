﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace eval_pendu
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        #region Members

        // Stores new ComboBox value selected
        private String CmbxTimeValue;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public Settings()
        {
            InitializeComponent();
            FillInTimeComboxBox(MainWindow.dictSettingsTimePerGame);
            CmbxTimeValue = CmbxTime.SelectedValue.ToString();
            // Add listener after combobox has been filled in and current value has been selected
            CmbxTime.SelectionChanged += CmbxTime_SelectionChanged;
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Fill ComboBox with supported duration values
        /// </summary>
        /// <param name="dict"></param>
        private void FillInTimeComboxBox(Dictionary<String, int> dict)
        {
            foreach(String setting in dict.Keys)
            {
                CmbxTime.Items.Add(setting);
            }

            // Select last value selected
            CmbxTime.SelectedValue = MainWindow.CmbxSettingsTimeSelectedValue;
        }

        #endregion

        #region Events
        /// <summary>
        /// Save the current duration selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmbxTime_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CmbxTimeValue = ((ComboBox)sender).SelectedValue.ToString();
        }

        /// <summary>
        /// Change the duration of a game when timed game is enabled
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.CmbxSettingsTimeSelectedValue = CmbxTimeValue;
            MainWindow.maxTimeForGuess = MainWindow.dictSettingsTimePerGame[CmbxTimeValue];
            MainWindow.labelTimeFormatStartTime = TimeSpan.FromSeconds(MainWindow.maxTimeForGuess).ToString();
            Close();
        }

        /// <summary>
        /// Don't change anything, just exit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        #endregion
    }
}
