﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.IO;
using System.Reflection;
using System.Windows.Threading;

namespace eval_pendu
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Members

        #region Resources Members
        /// <summary>
        /// Assembly access and start path for file resources
        /// </summary>
        // https://support.microsoft.com/en-us/help/319292/how-to-embed-and-access-resources-by-using-visual-c
        private Assembly _assembly;
        private StreamReader _textStreamReader;
        private String reader = "";

        private const String resourcesWordsListsFilenameStartWith = "FR_";
        private const String resourcesWordsListsFilenameEndWith = ".txt";
        private const String resourcesWordsListsNamespacePath = "eval_pendu.resources.wordslists.";
        private const String resourcesWordsListDefaultFilename = "FR_general.txt";  // if changed, file build action embedded resource + copy to output directory always !!!
        private const String cmbxRandomThemeDefault = "GENERAL";
        #endregion

        #region Collection Members
        /// <summary>
        /// Contains filename words lists resources
        /// </summary>
        private Dictionary<String, String> dictResourcesWordsListsFiles = new Dictionary<String, String>();

        /// <summary>
        /// Dictionary to store words from file, List and Arrays too slow
        /// </summary>
        private Dictionary<String, String> dictWordsListFromResources = new Dictionary<String, String>();

        /// <summary>
        /// Stores all played words in random mode in order to not play the same word twice
        /// </summary>
        private Dictionary<String, String> dictRandomWordsPlayed = new Dictionary<string, string>();

        /// <summary>
        /// Contains all Canvas Dynamic Components, those that are set to visible or hidden during the game
        /// List so we can display them "in order" during the game
        /// </summary>
        private List<Ellipse> lstCanvasDynamicComponents = new List<Ellipse>();

        /// <summary>
        /// Contains all Wrap Panel Letters Buttons
        /// Dictionary so we can access them easily based on their key (letter)
        /// </summary>
        private Dictionary<Char, Button> dictWrapLettersBtns = new Dictionary<Char, Button>();

        /// <summary>
        /// Mappings between special characters and "simple" characters
        /// </summary>
        private Dictionary<Char, Char> dictSpecialLetterMapping = new Dictionary<char, char>();

        #endregion

        #region Constraints Members

        /// <summary>
        /// Max and min word length authorized for TextBox input and File input
        /// </summary>
        private const int maxWordLength = 20;
        private const int minWordLength = 4;

        /// <summary>
        /// NbTries allowed before ending the game, based on the number of Canvas Dynamic Components available
        /// </summary>
        private int maxTriesAllowed;
        
        #endregion

        #region Random Instance

        /// <summary>
        /// Private random instance (https://stackoverflow.com/questions/2706500/how-do-i-generate-a-random-int-number-in-c)
        /// </summary>
        private readonly Random RandomInstance = new Random();

        #endregion

        #region Timer
        
        private string labelTimeFormatZero = "00:00:00";
        private DispatcherTimer t;

        #region Shared Settings Members and Properties

        public static int maxTimeForGuess = 60;
        public static string labelTimeFormatStartTime = TimeSpan.FromSeconds(maxTimeForGuess).ToString();

        // Holds possible time duration settings for the game
        public static Dictionary<String, int> dictSettingsTimePerGame = new Dictionary<string, int>();

        /// <summary>
        /// Sets values in Settings window time duration combobox
        /// </summary>
        private static String cmbxSettingsTimeSelectedValue = "1 minute";

        public static String CmbxSettingsTimeSelectedValue
        {
            get { return cmbxSettingsTimeSelectedValue; }
            set { cmbxSettingsTimeSelectedValue = value; }
        }

        #endregion

        #endregion

        #region Statistics Shared Members and Properties

        /// <summary>
        /// If game has started but not ended yet
        /// </summary>
        private static bool isGameRunning;

        public static bool IsGameRunning
        {
            get { return isGameRunning; }
            set { isGameRunning = value; }
        }

        #region Statistics Numbers

        /// <summary>
        /// Number of total game played for the current session
        /// </summary>
        private static int statsGlobalTotalNbGames;

        public static int StatsGlobalTotalNbGames
        {
            get { return statsGlobalTotalNbGames; }
            set { statsGlobalTotalNbGames = value; }
        }

        /// <summary>
        /// Number of games won for the current session
        /// </summary>
        private static int statsGlobalTotalWins;

        public static int StatsGlobalTotalWins
        {
            get { return statsGlobalTotalWins; }
            set { statsGlobalTotalWins = value; }
        }

        /// <summary>
        /// Number of games lost for the current session
        /// </summary>
        private static int statsGlobalTotalLosses;

        public static int StatsGlobalTotalLosses
        {
            get { return statsGlobalTotalLosses; }
            set { statsGlobalTotalLosses = value; }
        }

        /// <summary>
        /// Best guess time for a game won for the current session
        /// </summary>
        private static int statsGlobalBestTime;

        public static int StatsGlobalBestTime
        {
            get { return statsGlobalBestTime; }
            set { statsGlobalBestTime = value; }
        }

        /// <summary>
        /// Last word played
        /// </summary>
        private static String statsGlobalLastWordPlayed;

        public static String StatsGlobalLastWordPlayed
        {
            get { return statsGlobalLastWordPlayed;  }
            set { statsGlobalLastWordPlayed = value; }
        }

        #endregion

        #endregion

        #endregion

        #region Properties

        /// <summary>
        /// Used when Timer enabled in game
        /// </summary>
        private int currentTimeRemaining;

        public int CurrentTimeRemaining
        {
            get { return currentTimeRemaining; }
            set { currentTimeRemaining = value; }
        }

        /// <summary>
        /// If checkbox checked, timed game is activated
        /// </summary>
        private bool timedGameEnabled;

        public bool TimedGameEnabled
        {
            get { return timedGameEnabled; }
            set { timedGameEnabled = value; }
        }

        /// <summary>
        /// If resources cannot be found or if no valid word can be found in resource, random mode is disabled
        /// </summary>
        private bool randomGameEnabled;

        public bool RandomGameEnabled
        {
            get { return randomGameEnabled; }
            set { randomGameEnabled = value; }
        }

        /// <summary>
        /// Tracking the number of wrong guesses
        /// </summary>
        private int currentBadGuesses;

        public int CurrentBadGuesses
        {
            get { return currentBadGuesses; }
            set { currentBadGuesses = value; }
        }

        /// <summary>
        /// Current playing word
        /// </summary>
        private String currentWord;

        public String CurrentWord
        {
            get { return currentWord; }
            set { currentWord = value; }
        }

        /// <summary>
        /// When loading a resource related to a selected theme in random mode,
        /// used to verify there is valid words in it and if all valid words in that theme 
        /// have been played. If so, we propose to the user to reload that theme if it had 
        /// originally valid words
        /// </summary>
        private int currentThemeNbValidWordsInResource;

        public int CurrentThemeNbValidWordsInResource
        {
            get { return currentThemeNbValidWordsInResource; }
            set { currentThemeNbValidWordsInResource = value; }
        }

        // Keep track of the current letters guessed to determine when all have been found
        /// <summary>
        /// 
        /// </summary>
        private int currentRemainingCharToGuess;

        public int CurrentRemainingCharToGuess
        {
            get { return currentRemainingCharToGuess; }
            set { currentRemainingCharToGuess = value; }
        }

        /// <summary>
        /// Resource name of the current word list we are playing with
        /// </summary>
        private String currentWordsListResource;

        public String CurrentWordsListResource
        {
            get { return currentWordsListResource; }
            set { currentWordsListResource = value; }
        }
                
        #endregion

        #region Constructor

        public MainWindow()
        {
            InitializeComponent();

            // Default Words List to play with 
            CurrentWordsListResource = resourcesWordsListDefaultFilename;

            // Init Time Dictionary for Settings Window
            SettingsInitTimeDictionary();

            // Set timed game properties
            t = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Background, DisplayAndCheckRemainingTime, Dispatcher.CurrentDispatcher);
            t.IsEnabled = false;
            CurrentTimeRemaining = maxTimeForGuess;
            TimedGameEnabled = false;
            pgBarTiming.Maximum = maxTimeForGuess;

            // Set Statistics game properties
            StatisticsInitProperties();

            // Enable Random Game mode
            RandomGameEnabled = true;

            // Initialize Letter Mapping Dictionary
            InitDictSpecialLettersMapping();

            // Set the number of tries allowed based on the number of components in the list (or CanDynamicComponents.Children.Count)
            maxTriesAllowed = CanDynamicComponents.Children.Count;

            // Set the maximum word length allowed in TextBox TbxWordProposition
            TbxWordProposition.MaxLength = maxWordLength;

            // Set CurrentWord to an Empty String
            CurrentWord = "";
            
            // Load all CanDynamicComponents into a list
            foreach (Ellipse elem in CanDynamicComponents.Children)
            {
                lstCanvasDynamicComponents.Add(elem);
            }

            // Generate Wrap Panel Letter Buttons
            WrapLettersBtnsGenerate();

            // Generate Grid Answer Labels
            GridAnswerLblGenerate(maxWordLength);

            // Initialization actions for a new game
            NewGame();
        }

        #endregion

        #region Helpers

        #region Game Initialization Helpers

        #region Initialization Settings Time Dictionary

        private void SettingsInitTimeDictionary()
        {
            dictSettingsTimePerGame.Clear();
            dictSettingsTimePerGame.Add("30 secondes", 30);
            dictSettingsTimePerGame.Add("1 minute", 60);
            dictSettingsTimePerGame.Add("1 minute 30 secondes", 90);
            dictSettingsTimePerGame.Add("2 minutes", 120);
            dictSettingsTimePerGame.Add("2 minutes 30 secondes", 150);
            dictSettingsTimePerGame.Add("3 minutes", 180);
        }

        #endregion

        #region Initialization Statistics Properties

        /// <summary>
        /// Initialize Statistics Properties
        /// </summary>
        public static void StatisticsInitProperties()
        {
            StatsGlobalTotalNbGames = 0;
            StatsGlobalTotalWins = 0;
            StatsGlobalTotalLosses = 0;
            StatsGlobalBestTime = 0;
            StatsGlobalLastWordPlayed = "n/a";
        }

        #endregion

        #region New Game

        /// <summary>
        /// Initialization for a new game
        /// </summary>
        private void NewGame()
        {
            // Hide all canvas dynamic components
            foreach (Ellipse item in lstCanvasDynamicComponents)
            {
                item.Visibility = Visibility.Hidden;
            }

            // Reset Timing Parameters
            if (timedGameEnabled)
            {
                pgBarTiming.Value = maxTimeForGuess;
                LblTimingDisplayTime.Content = labelTimeFormatStartTime;
            } 
            else
            {
                pgBarTiming.Value = 0;
                LblTimingDisplayTime.Content = labelTimeFormatZero;
            }
            
            CurrentTimeRemaining = maxTimeForGuess;
            ChkbxTimingActivate.IsEnabled = true;
            pgBarTiming.Foreground = Brushes.ForestGreen;
            pgBarTiming.Maximum = maxTimeForGuess;

            // Reset bad guesses status
            CurrentBadGuesses = 0;

            // Reset letter count to guess to 0
            CurrentRemainingCharToGuess = 0;

            // Enable word proposition textbox and set its properties
            TbxWordProposition.Clear();
            TbxWordProposition.IsEnabled = true;

            // Disable word proposition button and enable random word button
            BtnWordProposition.IsEnabled = false;
            BtnWordRandom.IsEnabled = RandomGameEnabled;
            CmbxRandomTheme.IsEnabled = RandomGameEnabled;
            BtnWordRandom.Focus();

            // Show all Wrap Panel Letter Buttons and disable the wrap panel
            WrapLettersBtnShowAll();
            WrapLettersBtns.IsEnabled = false;

            // Reset Grid Anwser Labels Content and Visibility (based on previous word played so we don't need to reset the entire grid
            for (int i = 0; i < CurrentWord.Length; i++)
            {
                ((Label)GridAnswerLbl.Children[i]).Content = '_';
                ((Label)GridAnswerLbl.Children[i]).Visibility = Visibility.Hidden;
            }

            // Enable menu items while not playing
            MenuItemSettings.IsEnabled = true;

            // Game is not running
            IsGameRunning = false;
        }

        #endregion

        #region Start Game

        /// <summary>
        /// Initialize remaining common components for the user to start playing
        /// </summary>
        private void StartGame()
        {
            // Game is running
            IsGameRunning = true;

            // Set the number of characters to find
            CurrentRemainingCharToGuess = CurrentWord.Length;

            // Disable all buttons
            BtnWordProposition.IsEnabled = false; // not necessary because text is empty and input is ot valid anymore
            BtnWordRandom.IsEnabled = false;
            CmbxRandomTheme.IsEnabled = false;

            // Show a specific number of grid answer labels based on the current word length
            GridAnswerLblShow(CurrentWord.Length);

            // Enable Wrap Panel Letters Buttons
            WrapLettersBtns.IsEnabled = true;

            // Disable the checkbox during the game
            ChkbxTimingActivate.IsEnabled = false;

            if(TimedGameEnabled)
            {
                pgBarTiming.Value = maxTimeForGuess;

                // Enable Dispatcher to call DisplayAndCheckRemainingTime every second during the game
                t.IsEnabled = true;
            }
            else
            {
                pgBarTiming.Value = 0;
            }

            // Disable Menu Items during game
            MenuItemSettings.IsEnabled = false;

            // Increment the total number of games played for Statistics
            StatsGlobalTotalNbGames++;
        }

        #endregion

        #region End Game

        /// <summary>
        /// Actions to take when the game ends
        /// </summary>
        /// <param name="isWon"></param>
        /// <param name="msgToDisplay"></param>
        private void EndGame(bool isWon, String msgToDisplay)
        {
            if(!isWon)
            {
                StatsGlobalTotalLosses++;
                ShowSolution(CurrentWord);
            }
            else
            {
                StatsGlobalTotalWins++;
                if (timedGameEnabled)
                {
                    int timeElsapsed = maxTimeForGuess - (int)pgBarTiming.Value;
                    if(StatsGlobalBestTime == 0 || timeElsapsed < StatsGlobalBestTime) StatsGlobalBestTime = timeElsapsed;
                }
            }

            StatsGlobalLastWordPlayed = CurrentWord;

            MessageBox.Show(msgToDisplay, "Résultat", MessageBoxButton.OK, MessageBoxImage.Information);
            NewGame();
        }

        /// <summary>
        /// If game not won, show solution
        /// </summary>
        /// <param name="word"></param>
        private void ShowSolution(String word)
        {
            for(int i = 0; i < word.Length; i++)
            {
                ((Label)GridAnswerLbl.Children[i]).Content = word[i];
            }
        }

        #endregion

        #region Timed Game Handler

        /// <summary>
        /// Handle Display of Timing components and kepp track of game status during a timed-enabled game
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayAndCheckRemainingTime(object sender, EventArgs e)
        {
            LblTimingDisplayTime.Content = TimeSpan.FromSeconds(--CurrentTimeRemaining).ToString();
            pgBarTiming.Value = CurrentTimeRemaining;
            Double ratio = (Double)pgBarTiming.Value / maxTimeForGuess;
            pgBarTiming.Foreground = ratio < 0.1 ? Brushes.Red : ratio < 0.25 ? Brushes.Orange : Brushes.ForestGreen;
            if (CurrentTimeRemaining == 0)
            {
                t.IsEnabled = false;
                EndGame(false, "Temps alloué écoulé");
            }
        }

        #endregion

        #region Load Words List

        /// <summary>
        /// Load words from file
        /// </summary>
        /// <param name="filename"></param>
        private void LoadWordsList(bool reloadTheme = false)
        {
            // If the theme selected is not the current theme with all words played but a new one or maybe an empty one
            // Necessary because _textStreamReader cannot be repositionned at the beginning
            if (CurrentThemeNbValidWordsInResource == 0)
            {
                // read resource selected
                reader = _textStreamReader.ReadToEnd();
            }

            // Set theme variable or reset previously set theme variable (if reloading)
            CurrentThemeNbValidWordsInResource = 0;

            // Load words into dictionary
            String[] lines = reader.Split('\n');
            dictWordsListFromResources.Clear();
            dictWordsListFromResources.Add("", ""); // 1st elem empty because random will never generate 0
            // Dictionary used in order to avoid duplicates and because checking this on a list is way too slow

            foreach (String line in lines)
            {
                String word = line.Trim();

                if(IsValidWord(word))
                {
                    // Increment the number of valid words present in selected theme
                    CurrentThemeNbValidWordsInResource++;

                    // If we are reloading a theme (all words of that theme already played)
                    if(reloadTheme)
                    {
                        word = FormatWord(word);
                        if (dictRandomWordsPlayed.ContainsKey(word))
                            dictRandomWordsPlayed.Remove(word);
                        dictWordsListFromResources.Add(word, "");
                    }

                    // We don't load words already played (when reloading a previous used theme for example)
                    else if (!dictRandomWordsPlayed.ContainsKey(word))
                    {
                        word = FormatWord(word);
                        if (!dictWordsListFromResources.ContainsKey(word))
                            dictWordsListFromResources.Add(word, "");
                    }
                }
            }
        }

        #endregion

        #region Load Combobox Themes
        /// <summary>
        /// Load ComboBox with theme available for random mode
        /// </summary>
        private void LoadComboRandomTheme()
        {
            //https://stackoverflow.com/questions/289/how-do-you-sort-a-dictionary-by-value
            String[] themes = dictResourcesWordsListsFiles.Keys.OrderBy(key => key).ToArray();

            foreach (String theme in themes)
            {
                CmbxRandomTheme.Items.Add(theme);
            }
        }
        #endregion

        #region Set Random Current Word
        /// <summary>
        /// Load or reload resource words file. return true if current word set, false otherwise
        /// </summary>
        /// <returns></returns>
        private bool SetRandomCurrentWord()
        {
            if (randomGameEnabled) // not necessary because button deactivated if no words ==> to check
            {
                // If the words have not been loaded yet or all might have been already played
                if (dictWordsListFromResources.Count <= 1)
                {
                    LoadWordsList();
                }

                if (dictWordsListFromResources.Count > 1)
                {
                    bool nextWord = false;

                    do
                    {
                        int randomNum = GetRandomNumber(1, dictWordsListFromResources.Count); // if 12 elems, index up to 11
                        CurrentWord = dictWordsListFromResources.ElementAt(randomNum).Key;
                        dictWordsListFromResources.Remove(CurrentWord); // so the same word is not selected twice
                        nextWord = dictRandomWordsPlayed.ContainsKey(CurrentWord);

                    } while (nextWord); // so the same word is not played twice, regardless of the theme chosen

                    dictRandomWordsPlayed.Add(CurrentWord, "");

                    return true;
                }
                else // LoadWordsList hasn't loaded any words
                {
                    if(CurrentThemeNbValidWordsInResource == 0) { 
                        MessageBox.Show($"Aucun mot valide n'a été trouvé dans le theme sélectionné " +
                            $"({CmbxRandomTheme.SelectedValue.ToString()}), veuillez en choisir un autre.", 
                            "Erreur", 
                            MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else // There are valid words in the current theme resource but all have already been played
                    {
                        MessageBoxResult result = MessageBox.Show($"Tous les mots du theme sélectionné ont été joués " +
                            $"({CmbxRandomTheme.SelectedValue.ToString()}). Souhaitez-vous recharger ce theme ?",
                            "Information",
                            MessageBoxButton.YesNo, MessageBoxImage.Question);

                        if(result == MessageBoxResult.Yes)
                        {
                            if (ReloadCurrentTheme())
                            {
                                MessageBox.Show($"Theme { CmbxRandomTheme.SelectedValue.ToString()} rechargé.",
                                    "Rechargement effectué",
                                    MessageBoxButton.OK,
                                    MessageBoxImage.Information);

                                return SetRandomCurrentWord();
                            }
                            else // Unknown (or non anticipated) error occurs
                            {
                                MessageBox.Show($"Une erreur s''st produite lors du rechargement du theme { CmbxRandomTheme.SelectedValue.ToString()}." +
                                    "Veuillez en sélectionner un autre.",
                                    "Erreur de rechargement",
                                    MessageBoxButton.OK,
                                    MessageBoxImage.Error);
                            }
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Trying to reload words from a current empty theme (all words mighthave been already played)
        /// </summary>
        /// <returns></returns>
        private bool ReloadCurrentTheme()
        {
            LoadWordsList(true);
            return dictWordsListFromResources.Count > 1;
        }

        #endregion

        #region Get Random Number

        /// <summary>
        /// Get a random number
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        private int GetRandomNumber(int min, int max)
        {
            lock (RandomInstance)
            {
                return RandomInstance.Next(min, max);
            }
        }

        #endregion

        #endregion

        #region Resources Helpers

        /// <summary>
        /// Check if needed resource is accessible by the program
        /// </summary>
        /// <param name="resourceNamespacePath"></param>
        /// <returns></returns>
        private bool IsResourceAccessible(String resourcePath)
        {
            try
            {
                _assembly = Assembly.GetExecutingAssembly();
                _textStreamReader = new StreamReader(_assembly.GetManifestResourceStream(resourcePath));
                return true;
            }
            catch { }

            return false;
        }

        /// <summary>
        /// Set random mode to disabled (if no resource can be loaded)
        /// </summary>
        /// <param name="msg"></param>
        private void RandomModeDisabled(String msg)
        {
            BtnWordRandom.IsEnabled = false;
            RandomGameEnabled = false;
            CmbxRandomTheme.IsEnabled = false;
            TbxWordProposition.IsEnabled = true;
            TbxWordProposition.Focus();
            MessageBox.Show(msg, "Mode aléatoire désactivé", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        /// <summary>
        /// Load the name and path of all available Words Lists resources for random game mode
        /// </summary>
        /// <returns></returns>
        private int LoadResourcesWordsListsFilenames()
        {
            try
            {
                // Create an instance of assembly in order to get all embedded resources available in the assembly
                _assembly = Assembly.GetExecutingAssembly();
                
                // Get the list of all resources available
                String[] resources = _assembly.GetManifestResourceNames();

                foreach (String str in resources)
                {
                    // Select only wordslists files
                    if (str.Contains(resourcesWordsListsNamespacePath) && str.Length > resourcesWordsListsNamespacePath.Length)
                    {
                        String resourceName = str.Substring(resourcesWordsListsNamespacePath.Length);

                        if (resourceName.StartsWith(resourcesWordsListsFilenameStartWith) &&
                            resourceName.EndsWith(resourcesWordsListsFilenameEndWith) &&
                            IsResourceAccessible(resourcesWordsListsNamespacePath + resourceName))
                        {
                            int themeLength = resourceName.Length - resourcesWordsListsFilenameStartWith.Length - resourcesWordsListsFilenameEndWith.Length;
                            String theme = resourceName.Substring(resourcesWordsListsFilenameStartWith.Length).Substring(0, themeLength).ToUpper();
                            // Key is theme listed in combobox, resourceName is filename
                            dictResourcesWordsListsFiles.Add(theme, resourceName);
                        }
                    }
                }
            }

            catch (Exception e)
            {

                RandomModeDisabled($"Error initializing _assembly, random mode disabled");
            }

            return dictResourcesWordsListsFiles.Count;
        }

        #endregion       

        #region Wrap Panel Letters Buttons Helpers
        /// <summary>
        /// Generate WrapPanel Letters Buttons
        /// </summary>
        private void WrapLettersBtnsGenerate()
        {
            for (char ch = 'A'; ch <= 'Z'; ch++)
            {
                Button btn = new Button
                {
                    Content = ch,
                    Width = 75,
                    Height = 50,
                    FontSize = 20,
                    Padding = new Thickness(5),
                    Margin = new Thickness(5)
                };

                btn.Click += new RoutedEventHandler(WrapLetterBtn_Click);

                dictWrapLettersBtns.Add(ch, btn);
                WrapLettersBtns.Children.Add(btn);
            }
        }

        /// <summary>
        /// Display all Wrap Panel Letters Buttons
        /// </summary>
        private void WrapLettersBtnShowAll()
        {
            foreach(Button btn in dictWrapLettersBtns.Values)
            {
                btn.Visibility = Visibility.Visible;
            }
        }
        #endregion

        #region Grid Answer Labels Helpers

        /// <summary>
        /// Generate the new grid answer labels based on the size provided
        /// </summary>
        /// <param name="size"></param>
        private void GridAnswerLblGenerate(int size)
        {
            GridAnswerLbl.ColumnDefinitions.Clear();

            for (int i = 0; i < size; i++)
            {
                ColumnDefinition cd = new ColumnDefinition
                {
                    Width = new GridLength(1, GridUnitType.Auto)
                };

                GridAnswerLbl.ColumnDefinitions.Add(cd);
                Label lbl = new Label
                {
                    Content = '_',
                    FontSize = 30,
                    Width = 40,
                    Height = 50,
                    Padding = new Thickness(5, 5, 5, 2),
                    Visibility = Visibility.Hidden
                };

                lbl.SetValue(Grid.ColumnProperty, i);

                GridAnswerLbl.Children.Add(lbl);
            }
        }

        /// <summary>
        /// Display a specific number of grid answer labels
        /// </summary>
        /// <param name="nbToShow"></param>
        private void GridAnswerLblShow(int nbToShow)
        {
            for (int i = 0; i < nbToShow; i++)
            {
                ((Label)GridAnswerLbl.Children[i]).Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Display the good guess at the right position on the grid answer labels
        /// </summary>
        /// <param name="ch"></param>
        private void DisplayGridAnswerLbl(Char ch)
        {
            int startPos = 0;

            do {
                startPos = CurrentWord.IndexOf(ch, startPos);
                if(startPos > -1)
                {
                    ((Label)GridAnswerLbl.Children[startPos]).Content = ch;
                    startPos += 1; // start from the next character
                    CurrentRemainingCharToGuess--;
                }

            } while (startPos > -1);
        }

        #endregion

        #region Content Check and Format Helpers

        /// <summary>
        /// Checking to see if string is composed of letters only
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private bool IsValidWord(String text)
        {
            return !String.IsNullOrEmpty(text) && 
                text.Length >= minWordLength && 
                text.Length <= maxWordLength && 
                !text.Contains('µ') &&
                text.All(Char.IsLetter);
        }

        /// <summary>
        /// Format words so we can handle all cases in a standard way
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        private String FormatWord(String word)
        {
            String result = "";

            foreach(Char ch in word)
            {
                Char upper = Char.ToUpper(ch);
                if(upper < 'A' || upper > 'Z')
                {
                    try
                    {
                        result += dictSpecialLetterMapping[upper];
                    }
                    catch (Exception e)
                    {
                        throw new Exception($"FormatCurrentWord -- Letter {upper} not in dictSpecialLetterMapping");
                    }
                }
                else
                {
                    result += upper;
                }
            }

            return result;
        }

        /// <summary>
        /// Allows us to handle special characters
        /// </summary>
        private void InitDictSpecialLettersMapping()
        {
            dictSpecialLetterMapping.Add('Ä', 'A');
            dictSpecialLetterMapping.Add('À', 'A');
            dictSpecialLetterMapping.Add('Á', 'A');
            dictSpecialLetterMapping.Add('Â', 'A');
            dictSpecialLetterMapping.Add('Ã', 'A');

            dictSpecialLetterMapping.Add('Ç', 'C');

            dictSpecialLetterMapping.Add('Ë', 'E');
            dictSpecialLetterMapping.Add('È', 'E');
            dictSpecialLetterMapping.Add('É', 'E');
            dictSpecialLetterMapping.Add('Ê', 'E');

            dictSpecialLetterMapping.Add('Ï', 'I');
            dictSpecialLetterMapping.Add('Ì', 'I');
            dictSpecialLetterMapping.Add('Í', 'I');
            dictSpecialLetterMapping.Add('Î', 'I');

            dictSpecialLetterMapping.Add('Ñ', 'N');

            dictSpecialLetterMapping.Add('Ö', 'O');
            dictSpecialLetterMapping.Add('Ò', 'O');
            dictSpecialLetterMapping.Add('Ó', 'O');
            dictSpecialLetterMapping.Add('Ô', 'O');
            dictSpecialLetterMapping.Add('Õ', 'O');

            dictSpecialLetterMapping.Add('Ü', 'U');
            dictSpecialLetterMapping.Add('Ù', 'U');
            dictSpecialLetterMapping.Add('Ú', 'U');
            dictSpecialLetterMapping.Add('Û', 'U');

            dictSpecialLetterMapping.Add('Ý', 'Y');
        }

        #endregion

        #endregion

        #region Events

        #region On Load Window

        /// <summary>
        /// If files cannot be loaded, random option is disabled
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (RandomGameEnabled)
            {
                if (LoadResourcesWordsListsFilenames() < 1)
                {
                    RandomModeDisabled($"Aucune ressource disponible dans le namespace {resourcesWordsListsNamespacePath}, mode aléatoire désactivé.");
                }
                else
                {
                    LoadComboRandomTheme();
                    
                    // The listener is added to the combobox after the combo has been loaded with values
                    CmbxRandomTheme.SelectionChanged += CmbxRandomTheme_SelectionChanged;
                    
                    // Select default theme (triggers selected_changed event which loads correct resource)
                    int index = CmbxRandomTheme.Items.IndexOf(cmbxRandomThemeDefault);
                    CmbxRandomTheme.SelectedIndex = index;
                }
            }
        }

        #endregion

        #region Click on letters (from mouse or kbd) in Wrap Panel or Focus on Textbox if Wrap Panel disabled

        /// <summary>
        /// Actions to take when a click on a letter button in wrap panel occurs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WrapLetterBtn_Click(object sender, RoutedEventArgs e)
        {
            // get the letter chosen
            Char ch = (Char)((Button)sender).Content;

            // Hide letter clicked on
            dictWrapLettersBtns[ch].Visibility = Visibility.Hidden;

            // If letter chosen not in word, display next canvas dynamic component and increment bad guess
            if (!CurrentWord.Contains(ch))
            {
                // display the next canvas dynamic component
                lstCanvasDynamicComponents[CurrentBadGuesses++].Visibility = Visibility.Visible;

                // Check if maximum bad guesses have been reached and end the game + deactivate timer is so
                if (CurrentBadGuesses == maxTriesAllowed)
                {
                    if (TimedGameEnabled) t.IsEnabled = false;
                    EndGame(false, "Perdu");
                }
            }
            else
            {
                // Display the letter(s) found in the grid answer label at the right position(s)
                DisplayGridAnswerLbl(ch);

                // Check if all letters have been found and deactivate timer is so
                if(currentRemainingCharToGuess == 0)
                {
                    if (TimedGameEnabled) t.IsEnabled = false;
                    EndGame(true, "Gagné");
                }
            }
        }

        /// <summary>
        /// Wrap panel (letters) keyboard support and textbox focus if wrap panel not enabled
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            // If wrap panel is dsabled, the game hasn't started yet so we do nothing
            if (!WrapLettersBtns.IsEnabled)
            {
                TbxWordProposition.Focus();
                return;
            }

            String key = e.Key.ToString();

            // We make sure the length of the keycode is 1 (to avoid Left, Right, ...) and we check its presence in the dictionary
            if (key.Length == 1 && dictWrapLettersBtns.ContainsKey(key[0]))
            {
                Button btn = dictWrapLettersBtns[e.Key.ToString()[0]];
                // If the button hasn't been clicked on yet, we click
                if (btn.IsVisible)
                    WrapLetterBtn_Click(btn, null);
            }
        }

        #endregion

        #region Proposed Word Mode

        /// <summary>
        /// Check if text is valid and allow the game to start if so by enabling related button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TbxWordProposition_TextChanged(object sender, TextChangedEventArgs e)
        {
            BtnWordProposition.IsEnabled = IsValidWord(((TextBox)sender).Text);
            BtnWordProposition.IsDefault = BtnWordProposition.IsEnabled;
            BtnWordRandom.IsDefault = !BtnWordProposition.IsDefault;
        }

        /// <summary>
        /// Start the game with the proposed word
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnWordProposition_Click(object sender, RoutedEventArgs e)
        {
            // Disable input textbox
            TbxWordProposition.IsEnabled = false;

            // Set the word entered as the current word to play for the game
            CurrentWord = FormatWord(TbxWordProposition.Text);

            // Clear the entered word
            TbxWordProposition.Clear();

            // Ready to start game
            StartGame();
        }

        #endregion

        #region Random Mode

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnWordRandom_Click(object sender, RoutedEventArgs e)
        {
            // Clear and disable input textbox
            TbxWordProposition.Clear(); // in case the user wrote something in it
            TbxWordProposition.IsEnabled = false;
            CmbxRandomTheme.IsEnabled = false;

            // Select new word
            if(SetRandomCurrentWord())
            {
                StartGame();
            }
            // If a theme list is empty for example, NOT if the random mode has been disabled from start because it has no access to all resources
            else if(RandomGameEnabled)
            {
                CmbxRandomTheme.IsEnabled = true;
            }
        }

        /// <summary>
        /// Load new Words Lists when theme is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmbxRandomTheme_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            String selectedValue = ((ComboBox)sender).SelectedValue.ToString();

            if (dictResourcesWordsListsFiles.ContainsKey(selectedValue))
            {
                if (IsResourceAccessible(resourcesWordsListsNamespacePath + dictResourcesWordsListsFiles[selectedValue]))
                {
                    // Get and set selected resource filename
                    CurrentWordsListResource = dictResourcesWordsListsFiles[selectedValue];

                    // OK because LoadWordsList() is called when setting Current Random Word, if dict.count < 1
                    dictWordsListFromResources.Clear();

                    // New theme
                    CurrentThemeNbValidWordsInResource = 0;
                }
                else
                {
                    //RandomModeDisabled($"Error accessing resource theme {selectedValue}, random mode disabled");
                    MessageBox.Show($"Erreur lors de la tentative d'accès à la ressource liée au thème choisi ({selectedValue}). " + "" +
                        "Veuillez en sélectionner un autre.", 
                        "Erreur de chargement du thème",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }
            }
        }

        #endregion

        #region Timed Mode       

        /// <summary>
        /// Enable timed game
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChkbxTimingActivate_Checked(object sender, RoutedEventArgs e)
        {
            if (!ChkbxTimingActivate.IsEnabled) return;
            TimedGameEnabled = true;
            pgBarTiming.Value = maxTimeForGuess;
            LblTimingDisplayTime.Content = labelTimeFormatStartTime;
        }

        /// <summary>
        /// Disable timed game
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChkbxTimingActivate_Unchecked(object sender, RoutedEventArgs e)
        {
            if (!ChkbxTimingActivate.IsEnabled) return;
            TimedGameEnabled = false;
            pgBarTiming.Value = 0;
            LblTimingDisplayTime.Content = labelTimeFormatZero;
        }

        /// <summary>
        /// When click inside the grid, checkbox is checked or unchecked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridTiming_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (!ChkbxTimingActivate.IsEnabled) return;
            ChkbxTimingActivate.IsChecked = !ChkbxTimingActivate.IsChecked;
        }

        #endregion

        #region Open Settings Window and Update Settings when Closed

        private void MenuItemSettings_Click(object sender, RoutedEventArgs e)
        {
            Settings winSettings = new Settings();
            winSettings.ShowDialog();

            // Save Proposed Word value if any and update with new settings
            String tbxPropValue = TbxWordProposition.Text;
            NewGame();
            TbxWordProposition.Text = tbxPropValue;
            // Focus on Btnproposition if TbxWordProposition has valid input
            if (BtnWordProposition.IsEnabled) BtnWordProposition.Focus();
        }

        #endregion

        #region Open Statistics Window

        private void MenuItemStats_Click(object sender, RoutedEventArgs e)
        {
            Statistics winStats = new Statistics();
            winStats.ShowDialog();
        }

        #endregion

        #region Exit Application

        private void MenuItemExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        #endregion

        #endregion
    }
}
